/**
 * js para carregar as perguntras
 */

var index = {

    index: function () {
        var teste = this.formatData(this.perguntas);
        this.montarFormulario();
    },

    perguntas : [
        {
            "id": "1",
            "pergunta": "Qual seu -email?",
            "tipo": true,
            "texto": "Nao"
        },
        {
            "id": "1",
            "pergunta": "Qual seu -email?",
            "tipo": true,
            "texto": "Sim"
        },
        {
            "id": "1",
            "pergunta": "Qual seu -email?",
            "tipo": true,
            "texto": "talvez"
        },
        {
            "id": "2",
            "pergunta": "Gostou?",
            "tipo": true,
            "texto": "Nao sei"
        }
    ],

    formatData: function (data) {

        var ids = [];

      $.each(data, function (chave, valor) {
         ids.push(valor.id);
      });

      var perguntas = [];

      var ids_uniques = $.unique(ids);

      $.each(ids_uniques, function (chave, valor) {
          var texto = [];
          $.each(data, function (c, v) {
              if (valor === v.id) {
                  texto.push(v.texto);
              }
          });
          var pergunta = {"id": data[chave].id, 'pergunta': data[chave].pergunta, 'tipo': data[chave].tipo, 'texto': texto};
          perguntas.push(pergunta);
      });

      return perguntas;

    },

    montarFormulario: function () {
        //se tipo for true escolhe so uma reposta
        //se tipo for false escolhe uma ou mais resposta
        //se tipo for null pergunta aberta
        var teste = [
            {id: "1", pergunta: "Gostou do show?", texto: null, tipo: null},
            {id: "2", pergunta: "Como veio para o show?", texto: ['carro', 'moto', 'bus'], tipo: false},
            {id: "3", pergunta: "Foi fácil chegar ao show?", texto: ['sim', 'nao', 'mais ou menos'], tipo: true},
        ];


        this.carregarPergunta(teste);

    },

    criarCheckbox: function (nome, value) {
       return '<label> <input type="checkbox" name="'+value+'"/> ' +nome+ '</label>';
    },

    criarRadio: function (nome, value) {
       return '<label> <input type="radio" name="'+value+'"/> ' +nome+ '</label>';
    },

    criarInput: function (nome) {
        return '<input type="text" name="'+nome+'" class="form-control">';
    },

    carregarPergunta: function (dados) {
        var texto = null;
        $.each(dados, function (dadosChave, dadosValor) {
            texto = '<label>'+dadosValor.pergunta+'</label><br>';
            var value = dadosValor.id;
            if (dadosValor.texto !== null) {
                $.each(dadosValor.texto, function (chave, valor) {
                    if (dadosValor.tipo === true) {
                        texto = texto + index.criarRadio(valor, value) + '<br>';
                    } else if (dadosValor.tipo === false) {
                        texto = texto + index.criarCheckbox(valor, value) + '<br>';
                    }
                });
                texto = texto + '<br>' ;
                $('.carregar-formulario').append(texto);
            } else {
                texto = texto + index.criarInput(value) + '<br>' ;
                $('.carregar-formulario').append(texto);
            }
        });

    }

};