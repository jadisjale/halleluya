<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 14/07/2018
 * Time: 17:40
 */

include ('../Banco/Postgresql.php');

class RespostaPreSolicitadaDao
{

    private $db;

    /**
     * PerguntaDao constructor.
     */
    public function __construct()
    {
        $this->db = Database::conexao();
    }

    public function selectAll() {
        $stmt = $this->db->prepare("SELECT * FROM halleluya.resposta_pre_selecionada;");
        $stmt->execute();
        $values = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($values);
    }

    public function salvar ($post) {
        $texto = isset($post['texto']) ? $post['texto'] : null;
        $id_pergunta = isset($post['id_pergunta']) ? $post['id_pergunta'] : null;

        if (!$texto || !$id_pergunta) {
            $data = array (
                'codigo' => 9999,
                'data' => 'esta faltando algum parâmetro, verifique'
            );
            echo json_encode($data);
        } else {
            $sql = 'INSERT INTO halleluya.resposta_pre_selecionada (texto, id_pergunta) values (?, ?) RETURNING *';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array($texto, $id_pergunta));
            $data = array(
                'codigo' => 0,
                'data' => $stmt->fetchAll(PDO::FETCH_OBJ)
            );
            echo json_encode($data);
        }

    }

}