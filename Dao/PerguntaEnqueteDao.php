<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 14/07/2018
 * Time: 17:40
 */

include ('../Banco/Postgresql.php');

class PerguntaEnqueteDao
{

    private $db;

    /**
     * PerguntaDao constructor.
     */
    public function __construct()
    {
        $this->db = Database::conexao();
    }

    public function selectAll() {
        $stmt = $this->db->prepare("SELECT * FROM halleluya.pergunta_enquete;");
        $stmt->execute();
        $values = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($values);
    }

    public function salvar ($post) {

        $id_pergunta = isset($post['id_pergunta']) ? $post['id_pergunta'] : null;
        $id_resposta = isset($post['id_resposta']) ? $post['id_resposta'] : null;

        if (!$id_pergunta || !$id_resposta) {
            $data = array (
                'codigo' => 9999,
                'data' => 'esta faltando algum parâmetro, verifique'
            );
            echo json_encode($data);
        } else {

            try {
                $this->db->beginTransaction();

                $sql_enquete = 'INSERT INTO halleluya.enquete (data) VALUES (?) RETURNING *';
                $stmt_enquete = $this->db->prepare($sql_enquete);
                $stmt_enquete->execute(array(date("Y-m-d H:i:s")));
                $enquete = $stmt_enquete->fetch(PDO::FETCH_OBJ);

                $sql = 'INSERT INTO halleluya.pergunta_enquete (id_enquete, id_pergunta, id_resposta) VALUES (?, ?, ?) RETURNING *';
                $stmt = $this->db->prepare($sql);
                $stmt->execute(array($enquete->id, $id_pergunta, $id_resposta));
                $data = array(
                    'codigo' => 0,
                    'data' => $stmt->fetchAll(PDO::FETCH_OBJ)
                );
                $this->db->commit();
                echo json_encode($data);

            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage(), $e->getCode());
            }
        }

    }

}