<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 14/07/2018
 * Time: 17:40
 */

include ('../Banco/Postgresql.php');

class PerguntaDao
{

    private $db;

    /**
     * PerguntaDao constructor.
     */
    public function __construct()
    {
        $this->db = Database::conexao();
    }

    public function selectAll() {
        $stmt = $this->db->prepare("SELECT * FROM halleluya.pergunta");
        $stmt->execute();
        $values = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($values);
    }

    public function salvar ($post) {
        $pergunta = isset($post['pergunta']) ? $post['pergunta'] : null;
        $tipo = isset($post['tipo']) ? $post['tipo'] : null;

        if (!$pergunta || $tipo === null) {
            $data = array (
                'codigo' => 9999,
                'data' => 'esta faltando algum parâmetro, verifique'
            );
            echo json_encode($data);
        } else {
            $sql = 'insert into halleluya.pergunta (pergunta, tipo) values (?, ?) RETURNING *';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array($pergunta, $tipo));
            $data = array(
                'codigo' => 0,
                'data' => $stmt->fetchAll(PDO::FETCH_OBJ)
            );
            echo json_encode($data);
        }

    }

    public function perguntaResposta()
    {
        $query = "select pergunta.id, pergunta.pergunta, pergunta.tipo, resposta.texto from halleluya.pergunta as pergunta
                  inner join halleluya.resposta_pre_selecionada as resposta on resposta.id_pergunta = pergunta.id";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $values = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($values);

    }

}