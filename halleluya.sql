/*
Navicat PGSQL Data Transfer

Source Server         : postegresql
Source Server Version : 90603
Source Host           : localhost:5432
Source Database       : halleluya
Source Schema         : halleluya

Target Server Type    : PGSQL
Target Server Version : 90603
File Encoding         : 65001

Date: 2018-07-18 21:43:13
*/


-- ----------------------------
-- Sequence structure for enquete_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "halleluya"."enquete_id_seq";
CREATE SEQUENCE "halleluya"."enquete_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 5
 CACHE 1;
SELECT setval('"halleluya"."enquete_id_seq"', 5, true);

-- ----------------------------
-- Sequence structure for pergunta_enquete_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "halleluya"."pergunta_enquete_id_seq";
CREATE SEQUENCE "halleluya"."pergunta_enquete_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"halleluya"."pergunta_enquete_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for pergunta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "halleluya"."pergunta_id_seq";
CREATE SEQUENCE "halleluya"."pergunta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 5
 CACHE 1;
SELECT setval('"halleluya"."pergunta_id_seq"', 5, true);

-- ----------------------------
-- Sequence structure for resposta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "halleluya"."resposta_id_seq";
CREATE SEQUENCE "halleluya"."resposta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"halleluya"."resposta_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for resposta_pre_selecionada_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "halleluya"."resposta_pre_selecionada_id_seq";
CREATE SEQUENCE "halleluya"."resposta_pre_selecionada_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 6
 CACHE 1;
SELECT setval('"halleluya"."resposta_pre_selecionada_id_seq"', 6, true);

-- ----------------------------
-- Table structure for enquete
-- ----------------------------
DROP TABLE IF EXISTS "halleluya"."enquete";
CREATE TABLE "halleluya"."enquete" (
"id" int8 DEFAULT nextval('"halleluya".enquete_id_seq'::regclass) NOT NULL,
"data" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of enquete
-- ----------------------------
INSERT INTO "halleluya"."enquete" VALUES ('1', '2018-07-16 03:58:31');
INSERT INTO "halleluya"."enquete" VALUES ('3', '2018-07-16 03:59:07');
INSERT INTO "halleluya"."enquete" VALUES ('5', '2018-07-16 04:02:30');

-- ----------------------------
-- Table structure for pergunta
-- ----------------------------
DROP TABLE IF EXISTS "halleluya"."pergunta";
CREATE TABLE "halleluya"."pergunta" (
"id" int8 DEFAULT nextval('"halleluya".pergunta_id_seq'::regclass) NOT NULL,
"pergunta" varchar(255) COLLATE "default" NOT NULL,
"tipo" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of pergunta
-- ----------------------------
INSERT INTO "halleluya"."pergunta" VALUES ('1', 'Qual seu -email?', 't');
INSERT INTO "halleluya"."pergunta" VALUES ('2', 'Qual o espaço que gostou mais teste?', 't');
INSERT INTO "halleluya"."pergunta" VALUES ('3', 'Teste?', 't');
INSERT INTO "halleluya"."pergunta" VALUES ('4', 'Teste false?', 'f');
INSERT INTO "halleluya"."pergunta" VALUES ('5', 'Teste true?', 't');

-- ----------------------------
-- Table structure for pergunta_enquete
-- ----------------------------
DROP TABLE IF EXISTS "halleluya"."pergunta_enquete";
CREATE TABLE "halleluya"."pergunta_enquete" (
"id" int8 DEFAULT nextval('"halleluya".pergunta_enquete_id_seq'::regclass) NOT NULL,
"id_enquete" int8 NOT NULL,
"id_pergunta" int8 NOT NULL,
"id_resposta" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of pergunta_enquete
-- ----------------------------
INSERT INTO "halleluya"."pergunta_enquete" VALUES ('1', '5', '1', '1');

-- ----------------------------
-- Table structure for resposta
-- ----------------------------
DROP TABLE IF EXISTS "halleluya"."resposta";
CREATE TABLE "halleluya"."resposta" (
"id" int8 DEFAULT nextval('"halleluya".resposta_id_seq'::regclass) NOT NULL,
"resposta" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of resposta
-- ----------------------------
INSERT INTO "halleluya"."resposta" VALUES ('1', 'resposta 1');

-- ----------------------------
-- Table structure for resposta_pre_selecionada
-- ----------------------------
DROP TABLE IF EXISTS "halleluya"."resposta_pre_selecionada";
CREATE TABLE "halleluya"."resposta_pre_selecionada" (
"id" int2 DEFAULT nextval('"halleluya".resposta_pre_selecionada_id_seq'::regclass) NOT NULL,
"texto" varchar(255) COLLATE "default" NOT NULL,
"id_pergunta" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of resposta_pre_selecionada
-- ----------------------------
INSERT INTO "halleluya"."resposta_pre_selecionada" VALUES ('1', 'Sim', '1');
INSERT INTO "halleluya"."resposta_pre_selecionada" VALUES ('2', 'Nao', '1');
INSERT INTO "halleluya"."resposta_pre_selecionada" VALUES ('3', 'Nao', '1');
INSERT INTO "halleluya"."resposta_pre_selecionada" VALUES ('4', 'Nao', '1');
INSERT INTO "halleluya"."resposta_pre_selecionada" VALUES ('5', 'Nao', '1');
INSERT INTO "halleluya"."resposta_pre_selecionada" VALUES ('6', 'Nao', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "halleluya"."enquete_id_seq" OWNED BY "enquete"."id";
ALTER SEQUENCE "halleluya"."pergunta_enquete_id_seq" OWNED BY "pergunta_enquete"."id";
ALTER SEQUENCE "halleluya"."pergunta_id_seq" OWNED BY "pergunta"."id";
ALTER SEQUENCE "halleluya"."resposta_id_seq" OWNED BY "resposta"."id";
ALTER SEQUENCE "halleluya"."resposta_pre_selecionada_id_seq" OWNED BY "resposta_pre_selecionada"."id";

-- ----------------------------
-- Primary Key structure for table enquete
-- ----------------------------
ALTER TABLE "halleluya"."enquete" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table pergunta
-- ----------------------------
ALTER TABLE "halleluya"."pergunta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table resposta
-- ----------------------------
ALTER TABLE "halleluya"."resposta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table resposta_pre_selecionada
-- ----------------------------
ALTER TABLE "halleluya"."resposta_pre_selecionada" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "halleluya"."pergunta_enquete"
-- ----------------------------
ALTER TABLE "halleluya"."pergunta_enquete" ADD FOREIGN KEY ("id_pergunta") REFERENCES "halleluya"."pergunta" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "halleluya"."pergunta_enquete" ADD FOREIGN KEY ("id_resposta") REFERENCES "halleluya"."resposta" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "halleluya"."pergunta_enquete" ADD FOREIGN KEY ("id_enquete") REFERENCES "halleluya"."enquete" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
