<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 14/07/2018
 * Time: 16:57
 */

require ('../Dao/PerguntaDao.php');

$dao = new PerguntaDao();

try {
    switch ($_REQUEST['acao']) {
        case "select_all":
            $dao->selectAll();
            break;
        case "insert":
            $dao->salvar($_POST);
            break;
        case "pergunta_resposta":
            $dao->perguntaResposta();
            break;
        case 2:
            echo "i equals 2";
            break;
    }
} catch (Exception $e) {
    $array = array (
      'cod' => 9999,
      'data' => $e->getMessage()
    );
    echo json_encode($array);
}
